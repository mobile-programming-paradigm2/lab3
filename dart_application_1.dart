import 'dart:io';

void main(List<String> arguments) {
  bool end = false;
  while (!end) {
    print("---------------------------------------");
    print("1.+");
    print("2.-");
    print("3.*");
    print("4./");
    print("5.q");
    print("---------------------------------------");

    String? operator = stdin.readLineSync();
    switch (operator) {
      case "+":
        {
          String? first = stdin.readLineSync();
          double? a = double.parse(first!);
          String? second = stdin.readLineSync();
          double? b = double.parse(second!);
          print(a + b);
        }

        break;
      case "-":
        {
          String? first = stdin.readLineSync();
          double? a = double.parse(first!);
          String? second = stdin.readLineSync();
          double? b = double.parse(second!);
          print(a - b);
        }

        break;
      case "*":
        {
          String? first = stdin.readLineSync();
          double? a = double.parse(first!);
          String? second = stdin.readLineSync();
          double? b = double.parse(second!);
          print(a * b);
        }

        break;
      case "/":
        {
          String? first = stdin.readLineSync();
          double? a = double.parse(first!);
          String? second = stdin.readLineSync();
          double? b = double.parse(second!);
          print(a / b);
        }

        break;
      case "q":
        {
          print("0");
          end = true;
        }

        break;
    }
  }
}
